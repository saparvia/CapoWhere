#!/usr/bin/env python
"""
Copyright (c) 2010, Stefan Parviainen <pafcu@iki.fi>

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
"""

import sys
import re
from optparse import OptionParser

chord_regex = re.compile(r'(?<![\S])([A-G][#b]?[a-z0-9]*)(?!\S)')

from difficulties import difficulties, default_difficulty

# Function to reverse dictionary (nicer to access)
def reverse_dict(d):
	d2 = {}
	for k,v in d.items():
		for i in v:
			d2[i] = k
	return d2

difficulties = reverse_dict(difficulties)

# Slit chord notation into components
def parse_chord(chord):
	root = chord[0].upper()
	if root == 'H': root = 'B'
	if chord_regex.match(chord) == None:
		raise Exception

	if len(chord) < 2: return (root,'','')

	mod = ''
	rest_offset = 0
	if chord[1] == '#' or chord[1] == 'b':
		mod = chord[1]
		rest_offset = 1

	try: rest = chord[1+rest_offset:]
	except: pass

	return (root,mod,rest)

# Join chord components for nicer printing
def unparse_chord(chord):
	return ''.join(chord)

#Move chord up by half a step
def uphalf(x):
	root = x[0]
	mod = x[1]
	rest = x[2]

	if mod == 'b': chord = [root,'',rest]
	if mod == '':
		if root != 'B' and root != 'E':
			chord = [root,'#',rest]
		else:
			chord = [chr(ord(root)+1),'',rest]
	if mod == '#':
		if root != 'B' and root != 'E':
			if root == 'G':
				chord = ['A','',rest]
			else:
				chord = [chr(ord(root)+1),'',rest]
		else:
			chord = [chr(ord(root)+1),'#',rest]

	return chord

# Move chord up n halfsteps
def up(chord,n):
	for i in range(n):
		chord = uphalf(chord)
	return chord
	
# Determine diffuculty of chord
def difficulty(chord):
	return difficulties.get(chord,default_difficulty)

def best_transpose(chords):
	variations = []
	for i in range(12):
		variation = [unparse_chord(up(chord,i)) for chord in chords]
		variations.append((sum([difficulty(chord) for chord in variation]),variation,i))

	variations.sort()
	best = variations[0] # Determine best variation
	return best


if __name__ == '__main__':
	parser = OptionParser()
	parser.add_option("-a", "--aggressive", help="Handle everything that looks like a chord", dest="aggressive", action='store_true',default=False)
	(options, args) = parser.parse_args() 

	input = sys.stdin.read()
	lines = input.split('\n')
	# Parse chords
	chords = []
	if options.aggressive:
		chords = [parse_chord(x) for x in chord_regex.findall(input)]
	for line in lines:
		try:
			line_chords = [parse_chord(x) for x in line.split()]
			chords += line_chords
		except: # Just skip invalid lines
			pass

	# Determine possible variations and give them scores
	best = best_transpose(chords)
	# Print useful info
	if best[2] != 0:
		print 'Capo %s'%(12-best[2])
	else:
		print 'No capo needed'

	newchords = best[1] # Pick the best variation

	# Create a mapping from new to old chords
	pairs = set(zip([unparse_chord(x) for x in chords],newchords))
	lookup = {}
	for (k,v) in pairs:
		lookup[k] = v

	for line in lines:
		try:
			parts = line.split()
			if not options.aggressive:
				line_chords = [parse_chord(x) for x in parts]
			line = chord_regex.sub(lambda m: lookup[m.group(1)],line)
		except:
			pass
		print line
